import { React, expect, shallow } from '../test_helper';
import Banner from '../../src/components/home/banner';

describe('<Banner />', () => {
  let component;

  beforeEach(() => {
    component = shallow(<Banner/>);
  });

  it('renders the banner', () => {
    expect(component).to.have.className('app-home-banner');
  });

  it('has four banner components', () => {
    expect(component.find('.carousel-item')).to.have.length(4);
  })
});