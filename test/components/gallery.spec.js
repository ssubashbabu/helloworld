import { React, expect, shallow } from '../test_helper';
import Gallery from '../../src/components/gallery';

describe('<Gallery />', () => {
  let component;

  beforeEach(() => {
    component = shallow(<Gallery/>);
  });

  it('has four static image components', () => {
    expect(component.find('img')).to.have.length(4);
  })
});