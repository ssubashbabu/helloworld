import { React, shallow, expect } from '../test_helper';
import Content from '../../src/components/home/content';

describe('<Content />', () => {
  let component;

  beforeEach(() => {
    component = shallow(<Content/>);
  });

  it('should have defined class', () => {
    expect(component.find('.app-home-content')).to.have.length(1);
  });

  it('renders correct image', () => {
    expect(component.find('img')).to.have.attr('alt', 'Macaw');
  })
});