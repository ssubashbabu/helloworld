import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import jsdom from 'jsdom';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducers from '../src/reducers';
import { MemoryRouter } from 'react-router-dom';

const createRouterComponent = (Component, props = {}) => {
  return mount(
      <MemoryRouter>
        <Component { ...props }/>
      </MemoryRouter>
  );
};

const createComponent = (Component, props = {}) => {
  return mount(
      <Provider store={ createStore(rootReducers) }>
        <Component { ...props }/>
      </Provider>
  );
};

Enzyme.configure({ adapter: new Adapter() });
chai.use(new chaiEnzyme());

const { JSDOM } = jsdom;
const { document } = (new JSDOM('<!doctype html><html><body></body></html>')).window;

global.document = document;
global.window = document.defaultView;
global.navigator = global.window.navigator;

export {
  React,
  shallow,
  expect,
  createComponent,
  createRouterComponent
}