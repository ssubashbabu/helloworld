import React from 'react';

export default function () {
  return (
      <div className="container app-gallery">
        <h3> Gallery </h3>
        <div className="row">
          <div className="col-md-3 col-sm-6">
            <figure>
              <img src="../src/resources/images/bird1.jpg" alt="Top Oriole" title="Top Oriole"/>
              <figcaption>Top Oriole</figcaption>
            </figure>
          </div>
          <div className="col-md-3 col-sm-6">
            <figure>
              <img src="../src/resources/images/bird2.jpg" alt="Kingfisher" title="Kingfisher"/>
              <figcaption>Kingfisher</figcaption>
            </figure>
          </div>
          <div className="col-md-3 col-sm-6">
            <figure>
              <img src="../src/resources/images/bird3.jpg" alt="Piping Plover" title="Piping Plover"/>
              <figcaption>Piping Plover</figcaption>
            </figure>
          </div>
          <div className="col-md-3 col-sm-6">
            <figure>
              <img src="../src/resources/images/bird4.jpg" alt="Eagle" title="Eagle"/>
              <figcaption>Eagle</figcaption>
            </figure>
          </div>
        </div>
      </div>

  )
}