import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './components/home/home';
import Gallery from './components/gallery';
import Contact from './containers/contact';
import reducers from './reducers';
import Footer from './components/footer';
import Header from './containers/header';
import AboutUs from './components/about-us';

const createStoreWithMiddleware = applyMiddleware()(createStore);

ReactDOM.render(
    <Provider store={ createStoreWithMiddleware(reducers) }>
      <div>
        <Router>
          <div>
            <Header/>
            <Footer/>
            <Switch>
              <Route path="/about-us" component={ AboutUs }/>
              <Route path="/contact-us" component={ Contact }/>
              <Route path="/gallery" component={ Gallery }/>
              <Route path="/" component={ Home }/>
            </Switch>
          </div>
        </Router>
      </div>
    </Provider>
    , document.querySelector('.app-container'));
