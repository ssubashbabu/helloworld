import React, { Component } from 'react';
import { Field, reduxForm, handleSubmit, reset } from 'redux-form';

export const TextField = field => {
    const { meta: { touched, error } } = field;
    const className = `form-group  ${touched && error ? "error-message" : ""}`;
    return (
        <div className={className}>
            <input className="form-control" type="text" placeholder={field.placeholder} {...field.input} />
            <div className="text-help">
                {touched ? error : ""}
            </div>
        </div>
    )
};

export const TextAreaField = field => {
    const { meta: { touched, error } } = field;
    const className = `form-group  ${touched && error ? "error-message" : ""}`;
    return (
        <div className={className}>
            <textarea className="form-control" rows="6"
                placeholder={field.placeholder}
                {...field.input} />
            <div className="text-help">
                {touched ? error : ""}
            </div>
        </div>
    )
};

export class Contact extends Component {

    submitHandle(values) {
      // Form Submission action will be invoked here
    }
    render() {
        const { handleSubmit } = this.props;
        return (
            <div className="container app-contact-us">
                <h3> Contact Us </h3>
                <form onSubmit={handleSubmit(this.submitHandle.bind(this))}>
                    <div className="row">
                        <div className="col-sm-6">
                            <Field name="name" placeholder="Enter your name" component={TextField} />
                            <Field name="email" placeholder="Enter your e-mail address" component={TextField} />
                            <Field name="website" placeholder="Enter your website" component={TextField} /><br />
                        </div>
                        <div className="col-sm-6">
                            <Field name="message" placeholder="Enter your message" component={TextAreaField} />
                        </div>
                        <div className="col-sm-12">
                            <button type="submit" className="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

function validate(values) {
    const errors = {};
    if (!values.name) {
        errors.name = "Enter a name";
    }
    if (!values.email) {
        errors.email = "Enter a valid Email";
    }
    if (!values.website) {
        errors.website = "Enter a website";
    }
    if (!values.message) {
        errors.message = "Enter a message";
    }
    return errors;
}

const afterSubmit = (result, dispatch) =>
    dispatch(reset('contactForm'));

export default reduxForm({
    validate,
    form: 'contactForm',
    onSubmitSuccess: afterSubmit
})(Contact);