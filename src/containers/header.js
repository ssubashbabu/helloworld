import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

export const Menubar = ({ menuItems }) => {
  return menuItems.map((menuItem) => {
    const className = `nav-item`;
    return (
        <li
            className={ className }
            key={ menuItem.title }>
          <Link className="nav-link" to={ `/${menuItem.path}` }> { menuItem.title } </Link>
        </li>)
  })
};

class Header extends Component {
  render() {
    return (
        <div className="app-header">
          <nav className="navbar navbar-expand-md navbar-dark bg-dark">
            <Link className="navbar-brand" to="/">
              <img className="logo d-inline-block align-top" src="../src/resources/images/bird-logo.jpg" alt="Bird Life logo"/>
              Bird Life
            </Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavDropdown">
              <ul className="navbar-nav"><Menubar { ...this.props } /></ul>
            </div>
          </nav>
        </div>
    )
  }
}

function mapStatetoProps({ menuItems }) {
  return { menuItems }
}

export default connect(mapStatetoProps)(Header);