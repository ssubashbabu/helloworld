import { combineReducers } from 'redux';
import MenuItemReducer from './reducer-menu-items';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
  menuItems: MenuItemReducer,
  form: formReducer
});

export default rootReducer;
