# Project - Bird Life overview

As per the given template, I have selected the theme as birds.

**Note:** To suit the selected theme, I have changed the services menu item link to gallery.

## Framework and Testing

I have developed a single page application using React/Redux libraries to make highly dynamic, responsive and single responsibility components. 
I have written unit test cases for the components using Mocha and Chai. 

## Getting Started

### Pre-requisite

* Node.js - v8.0+

### Installation steps

```
git clone https://bitbucket.org/priyakbabu/birdlife-icmarkets.git  
cd birdlife-icmarkets  
npm install  
npm run start (for starting the project)  
npm run test (for executing test cases)  
```

### Project URL
```http://localhost:8080```
  
To run on a different port, update port number in **webpack.config.js**

### Technologies used
* HTML 5
* CSS
* Javascript
* React v16
* Redux 
* Redux form
* React router
* Bootstrap
### Testing frameworks
* Mocha
* Chai
* Enzyme
